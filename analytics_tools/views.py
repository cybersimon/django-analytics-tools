import random
from django.views.generic import TemplateView
from highcharts.views import HighChartsBarView
from analytics_tools.queries import top_countries


class AnalyticsView(TemplateView):
    template_name = "admin/analytics.html"


class ColumnView(HighChartsBarView):
    """
    A view that runs the passed in query, and returns results as JSON, in a
    format that can be digested by highcharts
    """
    categories = []
    chart_type = 'column'

    def __init__(self):
        super(ColumnView, self).__init__()
        self.results = top_countries('2014-01-01', '2014-07-30')

    def get_data(self):
        data = super(HighChartsBarView, self).get_data()
        for row in self.results['rows']:
            self.categories.append(row[0])
        data['xAxis']['categories'] = self.categories
        data['series'] = self.series
        return data

    @property
    def series(self):
        series = []
        data = []
        for row in self.results['rows']:
            data.append(int(row[1]))
        series.append({'name': 'Visits', 'data': data})
        return series


class BarExampleView(HighChartsBarView):
    categories = []
    chart_type = 'column'

    @property
    def series(self):
        result = []
        for name in ('Joe', 'Jack', 'William', 'Averell'):
            data = []
            # for x in range(len(self.categories)):
            data.append(random.randint(0, 10))
            result.append({"data": data})
        return result