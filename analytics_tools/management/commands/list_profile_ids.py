from django.core.management.base import BaseCommand

from ...connection import get_service

class Command(BaseCommand):
	help = 'List the Google Analytics Profile IDs associated with the available credentials.'
	
	def handle(self, *args, **options):
		service = get_service()
		accounts = service.management().accounts().list().execute()
		for account in accounts.get('items') or []:
			print 'Account name: %s' % account.get('name')
			properties = service.management().webproperties().list(
				accountId=account.get('id')
			).execute()
			for property in properties.get('items') or []:
				print '\tProperty name: %s' % property.get('name')
				profiles = service.management().profiles().list(
				accountId=account.get('id'),
				webPropertyId=property.get('id')
				).execute()
				for profile in profiles.get('items') or []:
					print '\t\tProfile ID for view "%s": %s' % (profile.get('name'), profile.get('id'))