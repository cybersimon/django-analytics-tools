"""
Template tags that pull in various bits of data and charts relating to
site analytics.
"""
from django import template
from analytics_tools.utils import exec_query_function


register = template.Library()


@register.inclusion_tag('analytics_tools/tabular_data.html')
def analytics_as_table(query_name, start_date, end_date):
    return {'results': exec_query_function(query_name, start_date, end_date)}



@register.inclusion_tag('analytics_tools/chart_data.html')
def analytics_as_chart():
    return {}