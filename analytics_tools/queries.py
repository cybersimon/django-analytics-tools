from analytics_tools.utils import run_query


def get_top_keywords(start_date, end_date):
    """Executes and returns data from the Core Reporting API.

    This queries the API for the top 25 organic search terms by visits.

    Returns:
    The response returned from the Core Reporting API.
    """
    return run_query(
        start_date, end_date, 'ga:sessions',
        'ga:source,ga:keyword', '-ga:sessions',
        'ga:medium==organic', 1, 25
    )


def visits_over_period(start_date, end_date):
    """Executes and returns data from the Core Reporting API.

    This queries the API for total number of sessions (visits) between 2 dates.

    Returns:
    The response returned from the Core Reporting API.
    """
    return run_query(start_date, end_date, 'ga:sessions')


def top_countries(start_date, end_date):
    """Executes and returns data from the Core Reporting API.

    This queries the API for the top 10 countries that sessions originated
    from over a period.

    Returns:
    The response returned from the Core Reporting API.
    """
    return run_query(
        start_date, end_date, 'ga:sessions',
        'ga:country', '-ga:sessions',
        None, 1, 10
    )