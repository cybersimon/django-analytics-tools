import importlib
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured

from googleapiclient.errors import HttpError
from oauth2client.client import AccessTokenRefreshError

from .connection import get_service


def connect(argv=[]):
    # Attempt to connect to google analytics, and return the service object
    # Authenticate and construct service.
    service, flags = connection.init(
        argv, 'analytics', 'v3', __doc__, settings.GA_SETTINGS_FILE,
        scope='https://www.googleapis.com/auth/analytics.readonly'
    )
    return service, flags


def exec_query_function(function, start_date, end_date):
    # take a string which is the path to a python module, name of function
    # to execute, and start and end date params, and try to execute the
    # function and return any results
    paths = []
    if hasattr(settings, 'ANALYTICS_MODULE'):
        # add user-defined module paths to the list
        for setting_path in settings.ANALYTICS_MODULE:
            paths.append(setting_path)
    # add our default path to the end
    # so it doesn't override the user-defined functions
    paths.append('analytics_tools.queries')
    # for each module defined in paths
    for path in paths:
        # try to get the module
        py_module = importlib.import_module(path)
        # Try to execute the named function in that module.
        try:
            return getattr(py_module, function)(start_date, end_date)
        except AttributeError:
            pass
    return None


def run_query(api='ga', **kwargs):
    service = get_service()
    
    if not getattr(settings, 'GOOGLE_ANALYTICS_PROFILE', None):
        raise ImproperlyConfigured(
            'Please provide a Google Analytics Profile ID in the '
            'GOOGLE_ANALYTICS_PROFILE setting'
        )
    profile_id = settings.GOOGLE_ANALYTICS_PROFILE

    api_defaults = {
        'ga': {
            'ids': 'ga:' + profile_id,
            'start_date': None,
            'end_date': None,
            'metrics': None,
            'dimensions': None,
            'sort': None,
            'filters': None,
            'start_index': 1,
            'max_results': 25,
        },
        'realtime': {
            'ids': 'ga:' + profile_id,
            'metrics': None,
            'dimensions': None,
            'sort': None,
            'max_results': 25,
        }
    }

    if api not in api_defaults:
        print('Unknown API: %s, using "ga" instead.' % api)

    defaults = api_defaults.get(api, api_defaults['ga'])

    unexpected = set(kwargs.keys()) - set(defaults.keys())
    if unexpected:
        print('Warning: received unexpected parameters: %s' %
            ', '.join(unexpected))
    defaults.update(kwargs)

    # Try to make a request to the API. Print the results or handle errors.
    try:
        return getattr(service.data(), api)().get(**defaults).execute()

    except TypeError, error:
        # Handle errors in constructing a query.
        print ('There was an error in constructing your query : %s' % error)

    except HttpError, error:
        # Handle API errors.
        print ('Arg, there was an API error : %s : %s' %
               (error.resp.status, error._get_reason()))

    except AccessTokenRefreshError:
        # Handle Auth errors.
        print ('The credentials have been revoked or expired, please re-run '
               'the application to re-authorize')


def print_results(results):
    """Prints results to stdout. Useful for debugging.

    This prints out the profile name, the column headers, and all the rows of
    data.

    Args:
    results: The response returned from the Core Reporting API.
    """
    print
    print 'Profile Name: %s' % results.get('profileInfo').get('profileName')
    print

    # Print header.
    output = []
    for header in results.get('columnHeaders'):
        output.append('%30s' % header.get('name'))
        print ''.join(output)

    # Print data table.
    if results.get('rows', []):
        for row in results.get('rows'):
            output = []
            for cell in row:
                output.append('%30s' % cell)
                print ''.join(output)
    else:
        print 'No Rows Found'

