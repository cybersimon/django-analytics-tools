from django.conf.urls import patterns, url
from django.contrib import admin
from analytics_tools.views import AnalyticsView, ColumnView, BarExampleView


urlpatterns = patterns('analytics_tools.views',
    (r'^$', admin.site.admin_view(AnalyticsView.as_view())),
    url(r'^column/$', admin.site.admin_view(ColumnView.as_view()), name='column'),
    url(r'^bar_example/$', admin.site.admin_view(BarExampleView.as_view()), name='bar'),
)

