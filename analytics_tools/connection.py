from django.conf import settings
from django.core.exceptions import ImproperlyConfigured

from oauth2client.client import GoogleCredentials
from googleapiclient.discovery import build

def get_service():
    if not getattr(settings, 'GOOGLE_OAUTH_CREDENTIALS', None):
        raise ImproperlyConfigured(
            'Please provide a path to a Google OAuth credentials JSON field in '
            ' the GOOGLE_OAUTH_CREDENTIALS setting'
        )
    credentials = GoogleCredentials.from_stream(settings.GOOGLE_OAUTH_CREDENTIALS)
    return build('analytics', 'v3', credentials=credentials)
