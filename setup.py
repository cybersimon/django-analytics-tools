from analytics_tools import VERSION
import setuptools

setuptools.setup(
    name='django-analytics-tools',
    version=VERSION,
    packages=setuptools.find_packages(),
    install_requires=[
     'django-classy-tags',
     'google-api-python-client',
    ],
)